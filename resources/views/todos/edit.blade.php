<h1>Create a new book</h1>
@extends('layouts.app')
@section('content')
<form method = 'post' action = "{{action('Todocontroller@update',$todo->id)}}" >
{{csrf_field()}}      
@method('PATCH')    
<div class = "form-group">    
    <label for = "title">Todo to update </label>
    <input type = "text" class = "form-control" name = "title" value = "{{$todo->title}}">
</div>


<div class = "form-group">    
    <input type = "submit" class = "form-control" name = "submit" value = "Update">
</div>

</form>

<form method = 'post' action = "{{action('Todocontroller@destroy',$todo->id)}}" >
{{csrf_field()}}      
@method('DELETE')    
<div class = "form-group">    
    <input type = "submit" class = "form-control" name = "submit" value = "Delete">
</div>

</form>

@endsection