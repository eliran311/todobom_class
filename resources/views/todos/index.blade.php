<h1>
This is your book list
</h1>
@extends('layouts.app')
@section('content')
<ul>
{{--comment --}}
@foreach($todos as $todo) 
<li>
@if ($todo->status)
           <input type = 'checkbox' id ="{{$todo->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$todo->id}}">
       @endif

 <a href = "{{route('todos.edit',$todo->id)}}">   {{$todo->title}} </a>
</li>
@endforeach
</ul>

<a href = "{{route('todos.create')}}">   create a new todo </a>
<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               $.ajax({
                   url:  {{url('todos')}}+ '/' + event.target.id,
                   dataType: 'json',
                   type:  'put',
                   contentType: 'aplication/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData: Should JQUERY change data to query string,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>  

@endsection