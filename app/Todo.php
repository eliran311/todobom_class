<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    //database fields to be allowed for massive assignment
    protected $fillable = ['title','status'];
    public function user()
   {
       return $this->belongsTo('App\User');
   }

}
