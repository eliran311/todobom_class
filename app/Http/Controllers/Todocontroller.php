<?php

namespace App\Http\Controllers;
use App\Todo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class Todocontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //get all todos
      // $todos = Todo::all();
       //return view('todos.index',['todos' => $todos]);
       {
        $id = Auth::id(); //or later change to 2
        $todos = User::find($id)->todos;
        return view('todos.index', compact('todos')); //compact(todos) equivalent to [‘todos’ => $todos]
    }
 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $todo = new Todo();
        $id = Auth::id();
        $todo->title = $request->title;//the last title from the form and the first from db
        $todo->user_id = $id;
        $todo->status = 0;
        $todo->save();
        return redirect('todos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $todo = Todo::find($id);
        return view('todos.edit',['todo'=>$todo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $todo = Todo::find($id);
        if($todo->user->id == Auth::id())return (redirect('todos'));
        
        $todo->update($request->except(['_token']));
        if ($request->ajax()){
            return response::json(array('result'=>'success','status'=>$request->status),200);
        }
        return redirect('todos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($todo->user->id == Auth::id())return (redirect('todos'));
        $todo = Todo::find($id);
        $todo->delete();
        return redirect('todos');
    }
}
