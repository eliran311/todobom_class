<?php

use Illuminate\Database\Seeder;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todos')->insert(
            [
              [
               'title' => 'Harry Poter1',
               'user_id' => '1',
               
               'created_at' => date('Y-m-d G:i:s'),
              ],
                  [ 
                   'title' => 'Harry Poter2',
                   
                   'user_id' => '1',
                   'created_at' => date('Y-m-d G:i:s'),
                  ],
                  [ 
                    'title' => 'Harry Poter3',
                    'user_id' => '3',
                    
                    'created_at' => date('Y-m-d G:i:s'),
                   ],
                   [ 
                    'title' => 'Harry Poter4',
                    
                    'user_id' => '4',
                    'created_at' => date('Y-m-d G:i:s'),
                   ],
                   [ 
                    'title' => 'Harry Poter5',
                    
                    'user_id' => '5',
                    'created_at' => date('Y-m-d G:i:s'),
                   ],
            ]);   
    }
}
