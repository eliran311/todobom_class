<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            DB::table('users')->insert(
                [
                    [
                        'name' => 'yoram',
                        'email' => 'yoram@gmail.com',
                        'password' =>'12345678',
                        'created_at' => date('Y-m-d G:i:s'),
                    ],
                    [
                     'name' => 'eli',
                     'email' => 'eli@gmail.com',
                     'password' =>'12345678',
                     'created_at' => date('Y-m-d G:i:s'),
                    ],
                    [
                     'name' => 'boob',
                     'email' => 'boob@jack.com',
                     'password' =>'12345678',
                     'created_at' => date('Y-m-d G:i:s'),
                    ],
                ]);
            }
    }
}
